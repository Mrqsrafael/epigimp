﻿using System.Drawing;
using System.Windows.Forms;

namespace epigimp
{
    public class Layers
    {
        public Bitmap bitmap;
        public string name;
        
        private bool _selected;
        private bool _rendered;

        
        #region Constructor

        public Layers(string fileName, string layerName)
        {
            Image img = new Bitmap(fileName);
            
            bitmap = new Bitmap(img);
            name = layerName;
        }
        
        public Layers(string layerName, int width = 800, int height = 600)
        {
            bitmap = new Bitmap(width, height);
            name = layerName;
        }
        
        public bool Selected
        {
            get => _selected;
            set => _selected = value;
        }
        
        public bool Rendered
        {
            get => _rendered;
            set => _rendered = value;
        }

        #endregion


        
    }
}