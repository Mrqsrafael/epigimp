﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace epigimp
{
    public partial class Form1 : Form
    {

        #region properties

        List<Point> currentLine = new List<Point>();
        Color currentColor = Color.Black;
        Brush currentBrushColor = new SolidBrush(Color.Black);
        Bitmap bitmap;
        Graphics g;
        Rectangle rect;
        int tool = 0;
        int x = -1;
        int y = -1;
        Point PrevPos;
        bool moving = false;
        Pen pen;
        int size = 10;
        float PictureScale = 1;
        int PicWidth;
        int PicHeight;

        private Bitmap mapDisplay;
        
        private List<Layers> _layers = new List<Layers>();

        #endregion
        
        public Form1() {

            
            mapDisplay = new Bitmap(800, 600);
            InitializeComponent();
            toolStripMenuItem39.MouseDown += new MouseEventHandler(rectangleMod);
            toolStripMenuItem38.MouseDown += new MouseEventHandler(ellipseMod);
            _layers.Add(new Layers(("layer " + _layers.Count)));
            _layers.Last().Rendered = true;
            _layers.Last().Selected = true;
            checkedListBox1.Items.Add(_layers.ElementAt(0).name, true);
            checkedListBox1.SelectedIndex = 0;
            checkedListBox1.SelectedItem = "layer 0";
            layerRightClickMenu.MouseClick += new MouseEventHandler(contextMenu_MouseDown);
            g = Graphics.FromImage(mapDisplay);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            pen = new Pen(currentColor, size);
            pen.StartCap = pen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            numericUpDown1.Maximum = 1000;
            numericUpDown1.Value = size;
            PicWidth = pictureBox8.Size.Width;
            PicHeight = pictureBox8.Size.Height;
            toolStripStatusLabel1.Text = "Canvas Size: " + pictureBox8.Size.Width + "x" + pictureBox8.Size.Height;
            toolStripStatusLabel3.Text = "Zoom: " + trackBar1.Value + "x";
        }

        private void rectangleMod(object sender, MouseEventArgs e)
        {
            switch (e.Button) 
            {
                case MouseButtons.Left:
                    {
                        tool = 7;
                    }
                break;
            }
        }

        private void ellipseMod(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    {
                        tool = 6;
                    }
                    break;
            }
        }

        #region ColorPicker
        private void CustomColoursBTN_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            currentColor = colorDialog1.Color;
            currentBrushColor = new SolidBrush(currentColor);
            panel1.BackColor = currentColor;
            pen = new Pen(currentColor, size);
            pen.StartCap = pen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
        }
        #endregion

        #region mouse move
        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            toolStripStatusLabel2.Text = "Cursor Position: " + e.X + ", " + e.Y;
            if (tool == 1)
            {
                if (moving && x != -1 && y != -1)
                {
                    // set the layer who will be modified
                    update_Selected();
                    
                    g.DrawLine(pen, new Point(x, y), e.Location);
                    x = e.X;
                    y = e.Y;

                }
            }

            else if (tool == 7 || tool == 6)
            {
                if (moving && x != -1 && y != -1)
                {
                    update_Selected();

                    x = e.X;
                    y = e.Y;
//                    pictureBox8.Image = _layers[0].bitmap;
                }
            }
            else if (tool == 5)
            {
                if (e.Button == MouseButtons.Left)
                {
                    update_Selected();
                    currentLine.Add(e.Location);
                    using (Graphics G = Graphics.FromImage(pictureBox8.Image))
                    {
                        G.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
                        G.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                        G.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;

                        using (Pen somePen = new Pen(Color.Transparent, size))
                        {
                            somePen.MiterLimit = size / 2;
                            somePen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
                            somePen.LineJoin = System.Drawing.Drawing2D.LineJoin.Round;
                            somePen.StartCap = System.Drawing.Drawing2D.LineCap.Round;
                            if (currentLine.Count > 1)
                                G.DrawCurve(somePen, currentLine.ToArray());
                        }

                    }
                    update_Selected();
                }
            }
        }

        private void Canvas_MouseDown(object sender, MouseEventArgs e)
        {
            if (tool == 1)
            {
                moving = true;
                x = e.X;
                y = e.Y;

                // set the layer who will be modified
//                update_Selected();
            }
            else if (tool == 5)
            {
                currentLine.Add(e.Location);
            }
            else if (tool == 7 || tool == 6)
            {
                moving = true;
                PrevPos.X = e.X;
                PrevPos.Y = e.Y;
                x = e.X;
                y = e.Y;
//                pictureBox8.Image = _layers[0].bitmap;
                
            }
        }

        private void Canvas_MouseUp(object sender, MouseEventArgs e)
        {
            if (tool == 1) {
                moving = false;
                x = -1;
                y = -1;
                
                // set the layer who will be modified
//                update_Selected();
            }
            else if (tool == 3)
            {
                FloodFill(returnSelectedLayer(), e.Location, returnSelectedLayer().GetPixel(e.X, e.Y), currentColor);
            }
            else if (tool == 7)
            {
                update_Selected();
                    
                g.FillRectangle(currentBrushColor, GetRect());
                moving = false;
                x = -1;
                y = -1;
                PrevPos.X = -1;
                PrevPos.Y = -1;
//                pictureBox8.Image = _layers[0].bitmap;
            }
            else if (tool == 6)
            {
                g.FillEllipse(currentBrushColor, GetRect());
                moving = false;
                x = -1;
                y = -1;
                PrevPos.X = -1;
                PrevPos.Y = -1;
                pictureBox8.Image = _layers[0].bitmap;
            }
        }
        #endregion

        #region zoom
        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            toolStripStatusLabel3.Text = "Zoom: " + trackBar1.Value + "x";
            SetScale((float)trackBar1.Value);
        }
        private void SetScale(float picture_scale)
        {
            // Set the scale.
            PictureScale = picture_scale;

            // Resize the PictureBox.
            pictureBox8.ClientSize = new Size(
                (int)(PicWidth * PictureScale),
                (int)(PicHeight * PictureScale));

            // Redraw.
            Size newSize = new Size((int)(PicWidth * PictureScale), (int)(PicHeight * PictureScale));
            Bitmap tmpImage1 = new Bitmap(_layers[0].bitmap, newSize);
            _layers[0].bitmap = tmpImage1;
            g = Graphics.FromImage(_layers[0].bitmap);
            pictureBox8.Image = tmpImage1;
        }
        #endregion

        #region fill functionality
        private static bool ColorMatch(Color a, Color b)
        {
            return (a.ToArgb() & 0xffffff) == (b.ToArgb() & 0xffffff);
        }

        private void FloodFill(Bitmap bmp, Point pt, Color targetColor, Color replacementColor)
        {
            update_Selected();
            if ((targetColor.A == replacementColor.A) && (targetColor.R == replacementColor.R) && (targetColor.G == replacementColor.G) && (targetColor.B == replacementColor.B))
            {
                return;
            }
            Stack<Point> pixels = new Stack<Point>();
            targetColor = bmp.GetPixel(pt.X, pt.Y);
            int y1;
            bool spanLeft, spanRight;
            pixels.Push(pt);

            while (pixels.Count != 0)
            {
                Point temp = pixels.Pop();
                y1 = temp.Y;
                while (y1 > 0 && bmp.GetPixel(temp.X, y1) == targetColor)
                {
                    y1--;
                }
                y1++;
                spanLeft = false;
                spanRight = false;
                while (y1 < bmp.Height && bmp.GetPixel(temp.X, y1) == targetColor)
                {
                    bmp.SetPixel(temp.X, y1, replacementColor);

                    if (!spanLeft && temp.X > 0 && bmp.GetPixel(temp.X - 1, y1) == targetColor)
                    {
                        pixels.Push(new Point(temp.X - 1, y1));
                        spanLeft = true;
                    }
                    else if (spanLeft && temp.X - 1 == 0 && bmp.GetPixel(temp.X - 1, y1) != targetColor)
                    {
                        spanLeft = false;
                    }
                    if (!spanRight && temp.X < bmp.Width - 1 && bmp.GetPixel(temp.X + 1, y1) == targetColor)
                    {
                        pixels.Push(new Point(temp.X + 1, y1));
                        spanRight = true;
                    }
                    else if (spanRight && temp.X < bmp.Width - 1 && bmp.GetPixel(temp.X + 1, y1) != targetColor)
                    {
                        spanRight = false;
                    }
                    y1++;
                }

            }
            update_Selected();
            return;
        }
        #endregion
        /*
        * region with all File Menu related functions
        */

        #region FileBar

        /*
         * function called when you click on open in the file menu
         * it will create a new layer with the size of the image
         */

        private void Open_MouseClick(object sender, MouseEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            dlg.Title = "Open Image";
            dlg.InitialDirectory = "d:\\";  
            dlg.Filter = "Image files (*.jpg *.png)|*.jpg;*.png|All Files (*.*)|*.*";  
            dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                _layers.Add(new Layers(dlg.FileName, ("layer " + _layers.Count)));
                _layers.Last().Rendered = true;
                checkedListBox1.Items.Add(_layers.Last().name, true);

                g = Graphics.FromImage(mapDisplay);

                for (int n = 0; n < _layers.Count ; n++) {
                    if (_layers[n].Rendered == true)
                        g.DrawImage(_layers[n].bitmap, 0, 0);
                }

                pictureBox8.Image = mapDisplay;
//                pictureBox8.SizeMode = PictureBoxSizeMode.Zoom;
//              g = Graphics.FromImage(bitmap);
/*                layers.Add(new Bitmap(dlg.FileName));
                layers[layers.Count - 1].MakeTransparent();
                pictureBox8.Image = layers[layers.Count - 1];
                g = Graphics.FromImage(layers[layers.Count - 1]);
                */
            }

        }

        #endregion
        
        #region ToolBarSelector
        private void ToolCursor_MouseClick(object sender, MouseEventArgs e)
        {
            tool = 0;
        }
        private void ToolBrush_MouseClick(object sender, MouseEventArgs e)
        {
            tool = 1;
        }
        private void ToolSelector_MouseClick(object sender, MouseEventArgs e)
        {
            tool = 2;
        }
        private void ToolBucket_MouseClick(object sender, MouseEventArgs e)
        {
            tool = 3;
        }
        private void ToolText_MouseClick(object sender, MouseEventArgs e)
        {
            tool = 4;
        }
        private void ToolShape_MouseClick(object sender, MouseEventArgs e)
        {
            contextMenuStrip1.Show(this, new Point(50, 350));
        }
        private void ToolEraser_MouseClick(object sender, MouseEventArgs e)
        {
            tool = 5;
        }
        #endregion

        private void pictureBox8_Paint(object sender, PaintEventArgs e)
        {
            if (rect != null && tool == 7)
            {
                e.Graphics.DrawRectangle(Pens.Black, GetRect());
            }
            else if (rect != null && tool == 6)
            {
                e.Graphics.DrawEllipse(Pens.Black, GetRect());
            }
        }

        private Rectangle GetRect()
        {
            rect = new Rectangle();
            rect.X = Math.Min(PrevPos.X, x);
            rect.Y = Math.Min(PrevPos.Y, y);
            rect.Width = Math.Abs(PrevPos.X - x);
            rect.Height = Math.Abs(PrevPos.Y - y);
            return rect;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            size = (int)numericUpDown1.Value;
            pen = new Pen(currentColor, size);
            pen.StartCap = pen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
        }

        /*
         * region with all Layers related function
         */
        
        #region LayerManagement
        
        /*
        * function used to set the pictureBox to the selected layer 
        */
        
        private void update_Selected()
        {
            foreach (var layer in _layers)
            {
                if (layer.Selected)
                {
                    g = Graphics.FromImage(layer.bitmap);
                    pictureBox8.Image = layer.bitmap;
                }
            }
        }

        private Bitmap returnSelectedLayer()
        {
            foreach (var layer in _layers)
            {
                if (layer.Selected)
                {
                    return (layer.bitmap);
                }
            }
            return (null);
        }

        /*
         *  function called when you press a button with the cursor inside checkedListBox1
         * Right Click : display layerRightClickMenu
         */

        private void checkedListBox1_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                {
                    layerRightClickMenu.Show(this, new Point(e.X, e.Y));
                    checkedListBox1.SelectedIndex = checkedListBox1.IndexFromPoint(e.X, e.Y);
                } 
                    break;
            }
        }

        /*
         * function called when you right click on the checkedListBox1
         * option : Delete
         *     - will delete the layer selected
         */
        
        private void contextMenu_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    {
                        if (checkedListBox1.SelectedIndex > 0)
                        {

                            // remove layer from checkedListBox and layerList
                            
                            var index = checkedListBox1.SelectedIndex;
                            
                            _layers.RemoveAt(checkedListBox1.SelectedIndex);

                            // reset the checkedListBox1 selection
                            
                            checkedListBox1.SetSelected(checkedListBox1.FindString("layer 0"), true);
                            checkedListBox1.SelectedIndex = 0;

                            // will select the first layer because it can be deleted
                            
                            _layers.First().Selected = true;
                            _layers.First().Rendered = true;
                            
                            checkedListBox1.Items.RemoveAt(index);

                            // display all layers selected

                            displayLayers();
                        }
                    }
                    break;
            }
            
        }
        
        
        /** function called when a state is updated in checkedListBox
         *  it will reset the display
         */
        
        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            // reset the checkedListBox and update the state of its items
            
            foreach (var layer in _layers)
            {
                layer.Selected = false;
                if (layer.name == checkedListBox1.SelectedItem.ToString())
                {
                    layer.Selected = true;
                    layer.Rendered = checkedListBox1.GetItemChecked(checkedListBox1.SelectedIndex);
                }
            }
            
            // display all layers selected
            
            displayLayers();
        }

        /** display all layers
         *  clear the pictureBox in White
         *  and then display all layers who are checked
         */
        
        private void displayLayers()
        {
            g = Graphics.FromImage(mapDisplay);
            g.Clear(Color.White);
            
            foreach (var t in _layers.Where(t => t.Rendered))
            {
                g.DrawImage(t.bitmap, 0, 0);
            }
            pictureBox8.Image = mapDisplay;
        }

        #endregion

        private void pictureBox8_Click(object sender, EventArgs e)
        {

        }
    }
}
